<?php
session_start();
if (($_SESSION['user_level']) <= 6) {
echo "<p>User Level: " . $_SESSION['user_login'] . "</p>";
}
?>

<!DOCTYPE html>
<html>
<head>
  <title>Main Page</title>

  <link rel="stylesheet" type="text/css" href="./css/mainpage.css">
  <center><img src ="./pictures/Logo.jpg" alt="Logo"></center>
</head>
<body>
<table class="navbar">
	<tr>
		<th><a class="active" href="http://filbey1.myweb.cs.uwindsor.ca/60334/project/mainpage.php">Home</a></th>
		<th><a href="http://filbey1.myweb.cs.uwindsor.ca/60334/project/catalogue.php">Book Catalogue</a></th>
		<th id="admin1"><a href="http://filbey1.myweb.cs.uwindsor.ca/60334/project/addbooks.html">Add Books</a></th>
		<th id="admin2"><a href="http://filbey1.myweb.cs.uwindsor.ca/60334/project/removebooks.html">Remove Books</a></th>
		<th id="user1"><a href="http://filbey1.myweb.cs.uwindsor.ca/60334/project/borrowbooks.html">Borrow Books</a></th>
		<th><a href="http://filbey1.myweb.cs.uwindsor.ca/60334/project/logout.php">Logout</a></th>
	</tr>
</table>

</br></br></br>

<p>
Welcome to the Filbey Public Libray, Please enjoy our Website </br></br>

 <center><img src ="./pictures/lib.jpg" alt="lib"></center>


<footer>
    © 2020 Filbey Public Library. All rights reserved.
  
  <br></br>
   <br>
    Contact:
    </br>
    <br>
    Email :filbey1@uwindsor.ca
    </br>
    <br>
    Phone: 111-111-1111
             
    </br>
   <br>
    Mission: 
    The Filbey Public Library enlightens and empowers people by providing diverse and dynamic pathways to literacy and learning.
    </br>
</footer>
</body>
</html>