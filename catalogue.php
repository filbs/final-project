<?php
session_start();

if (($_SESSION['user_level']) <= 2) {
	echo "<p>User Level: " . $_SESSION['user_login'] . "</p>";
} else{
	echo "Your user level is not granted access to this part of the site.";
	exit();
}

require_once "connect.php";
if ($conn->connect_error) die("Connection failed: " . $conn->connect_error);
?>

<!DOCTYPE html>
<html>
<head>
  <title>Book Catalogue</title>

  <link rel="stylesheet" type="text/css" href="./css/mainpage.css">
  <center><img src ="./pictures/Logo.jpg" alt="Logo"></center>
</head>

<body>
<table class="navbar">
	<tr>
		<th><a href="http://filbey1.myweb.cs.uwindsor.ca/60334/project/mainpage.php">Home</a></th>
		<th><a class="active" href="http://filbey1.myweb.cs.uwindsor.ca/60334/project/.php">Book Catalogue</a></th>
		<th><a href="http://filbey1.myweb.cs.uwindsor.ca/60334/project/.html">Add Books</a></th>
		<th><a href="http://filbey1.myweb.cs.uwindsor.ca/60334/project/removebooks.html">Remove Books</a></th>
		<th></th>><a href="http://filbey1.myweb.cs.uwindsor.ca/60334/project/borrowbooks.html">Borrow Books</a></th>
		<th><a href="http://filbey1.myweb.cs.uwindsor.ca/60334/project/logout.php">Logout</a></th>
	</tr>
</table>

</br>
<p>
<?php
  $query ="SELECT * from classics";
    $result = $conn->query($query);
    echo "<table>";
	echo "<tr><td><b>Book ID</b>:"."<td><b>Author</b>: "."<td><b>Title</b>: "."<td><b>Year</b>: "."<td><b>ISBN</b>: "."<td><b>Availability</b>: "."</tr>"."</br>";
    while ($row = mysqli_fetch_array($result)) {
        echo "<tr><td>".$row['id']."<td>".$row['author']."<td>".$row['title']."<td>".$row['year']."<td>".$row['isbn']."<td>".$row['availability']."</tr>"."</br>";
    }
	echo "</table>";
$conn->close();
?>
</br></br>
<p>
<footer></footer>
</body>
</html>